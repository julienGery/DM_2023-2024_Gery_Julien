#ifndef BASES H
#define BASES H

#include "librairies.h"

int operation(const char op, const int a, const int b);
int pgcd(int a, int b);
int nombreDeChiffres(int a);
bool tripletPythagoticien(const uint a, const uint b, const uint c);
uint question5(const uint N);
unsigned long questionOuverte(const uint n);
#endif