let egaux f1 f2 = abs_float (f1 -. f2) < Float.epsilon *. 16.

type article = {nom : string; prix : float}
type panier = article list

let cree_article (nom : string) (prix : float) =
    assert (prix >= 0. && nom <> "");
    {nom = nom; prix = prix}

let prix_article article = article.prix
let nom_article article = article.nom

let panier_vide () : (panier) = []
let est_vide panier = match panier with
            |[] -> true
            |_ -> false


let depose_article panier article = article::panier

let dernier_article (panier : panier) = match panier with
            |[] -> failwith "pas d'article"
            |h :: t -> h

let retire_article panier = match panier with
            |[] -> []
            |h :: t -> t

let rec nombre_articles panier = match panier with
            |[] -> 0
            |h :: t -> 1 + nombre_articles t

let rec prix_panier panier = match panier with
            |[] -> 0.
            |h :: t -> h.prix +. prix_panier t


