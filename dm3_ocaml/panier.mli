(* Type représentant un article *)
type article = { nom : string; prix : float }

(* Type représentant un panier, qui est une liste d'articles *)
type panier = article list

(* Crée un nouvel article avec un nom et un prix constructeur O(1)*)
val cree_article : string -> float -> article

(* Retourne le prix d'un article O(1)*)
val prix_article : article -> float

(* Retourne le nom d'un article O(1)*)
val nom_article : article -> string

(* Crée un panier vide constructeur O(1)*)
val panier_vide : unit -> panier

(* Vérifie si un panier est vide O(1)*)
val est_vide : panier -> bool

(* Ajoute un article à un panier O(1)*)
val depose_article : panier -> article -> panier

(* Retourne le dernier article ajouté au panier O(1)*)
val dernier_article : panier -> article

(* Retire le dernier article ajouté au panier O(1)*)
val retire_article : panier -> panier

(* Compte le nombre d'articles dans un panier O(n)*)
val nombre_articles : panier -> int

(* Calcule le prix total du panier O(n)*)
val prix_panier : panier -> float
