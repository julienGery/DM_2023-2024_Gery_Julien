let coupe_line (line : string) (a : char) =
    let index = String.index line a in
    String.sub line 0 (index), String.sub line (index + 1) (String.length line - index - 1)


let depuis_fichier fichier =
    let ic = open_in fichier in
    let panier = ref (Panier.panier_vide ()) in
    try
        while true do
            let line = input_line ic in
            let prix, nom = coupe_line line ' ' in
            let prix = float_of_string prix in
            let article = Panier.cree_article nom prix in
            panier := Panier.depose_article !panier article;
        done;
        !panier
    with  End_of_file -> !panier


let est_solution (p : Panier.panier) b s =
    let n = Panier.nombre_articles p in
    let total = ref 0. in
    for i = 0 to n - 1 do
        if s land (1 lsl i ) != 0 then begin
            let prix = Panier.prix_article (Panier.dernier_article p) in
            total := !total +. prix;
        end;
        p = Panier.retire_article p
    done;

    if total = b then true else false

let resolution_naive panier b =
    let n = Panier.nombre_articles panier in
    let rec ox s = match s with
        |(-1) -> None
        |s -> if est_solution panier b s then Some s else ox (s - 1) in

    ox (1 lsl n - 1)

(*15 : on teste tout les entiers entre 0 et 2^n - 1 donc 2^n solutions à tester : O(2^n)*)
(*16 : a faire *)


let partition p =
    let p1 = ref (Panier.panier_vide ()) in
    let p2 = ref (Panier.panier_vide ()) in

    let rec ox l n = match l with
        |[] -> ();
        |h :: t -> if n mod 2 = 0 then p1 := Panier.depose_article !p1 h else p2 := Panier.depose_article !p2 h;
                   ox t (n + 1) in
    ox p 0;
    !p1, !p2


let rec insere l f = match l with
    |[] -> [f]
    |h :: t -> if f <= h then f::h::t else h::(insere t f)

(* let toutes_sommes panier = *)
    (* let sommes = ref [] in *)

    (* let rec ox l = *)

let renverse l =
    let rec ox l1 l2 =
        match l1 with
            |[] -> l2
            |a :: l -> ox l (a :: l2) in
    ox l []



