/*
CORRIGÉ PARTIEL DU TP : STRUCTURES RÉCURSIVES EN C
Par J. BENOUWT
Licence CC BY-NC-SA
*/

/*---------------------------------------------------------*/

#include "liste.h"

/*---------------------------------------------------------*/

/* Implémentation d'une liste chaînée */


struct maillon_s {
    contenu valeur;
    struct maillon_s* suivant;
};


// Interface principale

liste creer_liste() {
    return NULL;
}

bool est_vide_liste(liste l) {
    return l == NULL;
}

void ajouter_tete_liste(contenu val, liste* l) {
    assert(l != NULL);
    maillon* m = malloc(sizeof(maillon));
    assert(m != NULL);
    m->valeur = val;
    m->suivant = *l;
    *l = m;
}

contenu tete_liste(liste l) {
    assert(!est_vide_liste(l));
    return l->valeur;
}

liste queue_liste(liste l) {
    assert(!est_vide_liste(l));
    return l->suivant;
}

void detruire_liste(liste l) {
    if (!est_vide_liste(l)) {
        detruire_liste(l->suivant);
        free(l);
    }
}


// Fonctions complémentaires

contenu acceder_indice_liste(liste l, int indice) {
    assert(!est_vide_liste(l));
    assert(indice >= 0);
    if (indice == 0) {
        return tete_liste(l);
    }
    else {
        return acceder_indice_liste(queue_liste(l), indice-1);
    }
}

void modifier_indice_liste(liste l, int indice, contenu val) {
    assert(!est_vide_liste(l));
    assert(indice >= 0);
    if (indice == 0) {
        l->valeur = val;
    }
    else {
        modifier_indice_liste(queue_liste(l), indice-1, val);
    }
}

int longueur_liste(liste l) {
    if (est_vide_liste(l)) {
        return 0;
    }
    else {
        return 1 + longueur_liste(queue_liste(l));
    }
}

bool appartient_liste(liste l, contenu val) {
    if (est_vide_liste(l)) {
        return false;
    }
    else {
        return tete_liste(l) == val || appartient_liste(queue_liste(l), val);
    }
}

void afficher_liste_aux(liste l) {
    printf("%d", tete_liste(l));
    if (!est_vide_liste(queue_liste(l))) {
        printf(", ");
        afficher_liste_aux(queue_liste(l));
    }
}
void afficher_liste(liste l) {
    printf("[");
    if (!est_vide_liste(l)) {
        afficher_liste_aux(l);
    }
    printf("]\n");
}

bool egalite_liste(liste l1, liste l2) {
    if (est_vide_liste(l1)) {
        return est_vide_liste(l2);
    }
    if (est_vide_liste(l2)) {
        return false;
    }
    return tete_liste(l1) == tete_liste(l2) && egalite_liste(queue_liste(l1), queue_liste(l2));
}

liste copie_liste(liste l) {
    if (est_vide_liste(l)) {
        return creer_liste();
    }
    else {
        liste copie_queue = copie_liste(queue_liste(l));
        ajouter_tete_liste(tete_liste(l), &copie_queue);
        return copie_queue;
    }
}

bool est_triee_liste(liste l) {
    if (est_vide_liste(l) || est_vide_liste(queue_liste(l))) {
        return true;
    }
    else {
        return tete_liste(l) <= tete_liste(queue_liste(l)) && est_triee_liste(queue_liste(l));
    }
}


// Plus de fonctionnalités

void inserer_indice_liste(liste* l, contenu val, int indice) {
    assert(l != NULL);
    assert(indice >= 0);
    if (indice == 0) {
        ajouter_tete_liste(val, l);
    }
    else {
        assert(!est_vide_liste(*l));
        inserer_indice_liste(&(*l)->suivant, val, indice-1);
    }
}

void supprimer_indice_liste(liste* l, int indice) {
    assert(l != NULL);
    assert(indice >= 0);
    if (indice == 0) {
        liste q = queue_liste(*l);
        free(*l);
        *l = q;
    }
    else {
        supprimer_indice_liste(&(*l)->suivant, indice-1);
    }
}

liste concatener_liste(liste l1, liste l2) {
    if (est_vide_liste(l1)) {
        return copie_liste(l2);
    }
    else {
        liste queue = concatener_liste(queue_liste(l1), l2);
        ajouter_tete_liste(tete_liste(l1), &queue);
        return queue;
    }
}

liste tableau_vers_liste(contenu tab[], int taille) {
    if (taille == 0) {
        return creer_liste();
    }
    else {
        liste queue = tableau_vers_liste(tab + 1, taille - 1);
        ajouter_tete_liste(tab[0], &queue);
        return queue;
    }
}

void remplir_tab_a_partir_de_indice(contenu tab[], int indice, liste l) {
    if (!est_vide_liste(l)) {
        tab[indice] = tete_liste(l);
        remplir_tab_a_partir_de_indice(tab, indice + 1, queue_liste(l));
    }
}
contenu* liste_vers_tableau(liste l) {
    int taille = longueur_liste(l);
    contenu* tab = malloc(taille * sizeof(contenu));
    remplir_tab_a_partir_de_indice(tab, 0, l);
    return tab;
}

liste renverser_liste(liste l) {
    if (est_vide_liste(l)) {
        return creer_liste(l);
    }
    else {
        liste tete = creer_liste();
        ajouter_tete_liste(tete_liste(l), &tete);
        liste queue_renversee = renverser_liste(queue_liste(l));
        liste concatenation = concatener_liste(queue_renversee, tete);
        detruire_liste(tete);
        detruire_liste(queue_renversee);
        return concatenation;
    }
}


// Il faut impérativement savoir implémenter une liste chaînée rapidement
// (creer, detruire, ajouter_tete, est_vide, tete, queue) car
// - on en aura besoin assez souvent
// - l'implémentation de l'interface de base est rapide à écrire, ça ne m'étonnerait pas de la voir dans beaucoup de sujets de concours !

