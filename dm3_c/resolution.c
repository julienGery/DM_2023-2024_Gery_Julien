#include "liste.h"
#include "librairies.h"

//1
int resolution_simple(liste l)
{
    int max = tete_liste(l);
    int res = max;
    int len = longueur_liste(l);
    for (int i = 0; i < len; i += 1)
    {
        int n = acceder_indice_liste(l, i);
        if (max < n)
            max = n;
    }
    for (int i = 0; i < len; i += 1)
    {
        int n = acceder_indice_liste(l, i);
        if (res < n && n != max)
            res = n;
    }
    return res;
}
// 2 Complexite : Les 2 boucles sont O(n) donc resolution_simple est O(n)

//3
void partage(liste l, liste *l1, liste *l2)
{
    for (int i = 0; !est_vide_liste(l); i += 1)
    {
        if (i % 2 == 0)
            ajouter_tete_liste(tete_liste(l), l1);
        else
            ajouter_tete_liste(tete_liste(l), l2);
        l = queue_liste(l);
    }
    renverser_liste(*l1);
    renverser_liste(*l2);
}

//4
int resolution_diviser_pour_regner(liste l)
{
    liste l1 = creer_liste();
    liste l2 = creer_liste();
    partage(l, &l1, &l2);
    int max1 = tete_liste(l1);
    int max2 = tete_liste(l2);
    int res = max1;
    liste l1sauv = l1;
    liste l2sauv = l2;

    // cherche les max respectifs des 2 listes
    while (!est_vide_liste(l1))
    {
        int t = tete_liste(l1);
        if (max1 < t)
            max1 = t;
        l1 = queue_liste(l1);
    }
    while (!est_vide_liste(l2))
    {
        int t = tete_liste(l2);
        if (max1 < t)
            max1 = t;
        l2 = queue_liste(l2);
    }

    int maxa, maxb;
    liste la; // la liste dans laquelle il faut chercher le resultat en priorite
    if (max1 < max2)
    {
        maxa = max2;
        maxb = max1;
        la = l2sauv;
    }
    else
    {
        maxa = max1;
        maxb = max2;
        la = l1sauv;
    }

    // on cherche le second max dans la liste qui contient le max
    while (!est_vide_liste(la))
    {
        int t = tete_liste(la);
        if (res < t && t < maxa)
            res = t;
        la = queue_liste(la);
    }

    if (res < maxb) // le second max est dans la liste b donc le res est son max
        res = maxb;

    return res;
}
/* 5
Comparaisons :
- boucle max1 : n/2 = 2^{k-1}
- boucle max2 : n/2 = 2^{k-1}
- choix la : 1
- recherche second max : n/2 = 2^{k-1}
- verification rex < maxb : 1

---> Total : 3/2 * n + 2    =   3 * 2^{k-1} + 2
*/



struct liste_couples_s
{
    int vainqueur;
    liste vaincus;
    struct liste_couples_s *suivant;
};
typedef struct liste_couples_s liste_couples;
typedef liste_couples *tournoi;

/* 6
n = 2^k
- 1e tournoi : 2^{k-1}
- 2e : 2^{k-2}
...
- ke : 1

On somme = 2^k - 1 = n - 1 comparaisons
*/

//7
tournoi prepare_tournoi(liste l)
{
    tournoi t = malloc(longueur_liste(l)*sizeof(liste_couples));
    for (int i = 0; !est_vide_liste(l); i += 1)
    {
        t[i].vainqueur = tete_liste(l);
        t[i].vaincus = creer_liste();
        l = queue_liste(l);
    }
    return t;
}

//8
void detruire_tournoi(tournoi t)
{
    free(t);
}

//9
void partage_tournoi(tournoi l, tournoi* l1, tournoi* l2)
{
    *l1 = NULL;
    *l2 = NULL;

    for (int i = 0; l != NULL; i += 1)
    {
        liste_couples *couple = malloc(sizeof(liste_couples));

        couple->vainqueur = l->vainqueur;
        couple->vaincus = l->vaincus;
        couple->suivant = NULL;

        if (i % 2 == 0) {
            couple->suivant = *l1;
            *l1 = couple;
        } else {
            couple->suivant = *l2;
            *l2 = couple;
        }

        l = l->suivant;
    }
}

//10
tournoi renverser_tournoi(tournoi t)
{
    tournoi res = NULL;
    while (t != NULL)
    {
        liste_couples *couple = malloc(sizeof(liste_couples));
        couple->vainqueur = t->vainqueur;
        couple->vaincus = t->vaincus;
        couple->suivant = res;
        res = couple;
        t = t->suivant;
    }
    return res;
}

tournoi un_tour(tournoi l1, tournoi l2)
{
    tournoi res = NULL;

    while (l1 != NULL && l2 != NULL)
    {
        //Compare les premiers elements de l1 et l2
        if (l1->vainqueur > l2->vainqueur)
        {
            liste_couples *couple = malloc(sizeof(liste_couples));
            couple->vainqueur = l1->vainqueur;
            couple->vaincus = l1->vaincus;
            couple->suivant = res;
            res = couple;
            l1 = l1->suivant;
        }
        else
        {
            liste_couples *couple = malloc(sizeof(liste_couples));
            couple->vainqueur = l2->vainqueur;
            couple->vaincus = l2->vaincus;
            couple->suivant = res;
            res = couple;
            l2 = l2->suivant;
        }
    }

    //Ajoute le reste de l1
    while (l1 != NULL)
    {
        liste_couples *couple = malloc(sizeof(liste_couples));
        couple->vainqueur = l1->vainqueur;
        couple->vaincus = l1->vaincus;
        couple->suivant = res;
        res = couple;
        l1 = l1->suivant;
    }

    //Ajoute le reste de l2
    while (l2 != NULL)
    {
        liste_couples *couple = malloc(sizeof(liste_couples));
        couple->vainqueur = l2->vainqueur;
        couple->vaincus = l2->vaincus;
        couple->suivant = res;
        res = couple;
        l2 = l2->suivant;
    }

    res = renverser_tournoi(res);
    return res;
}

//11
int longueur_tournoi(tournoi t)
{
    int res = 0;
    tournoi t2 = t;
    while (t2 != NULL)
    {
        res += 1;
        t2 = t2->suivant;
    }
    return res;
}

tournoi tous_les_tours(tournoi t)
{
    while(longueur_tournoi(t) > 1)
    {
        tournoi t1, t2;
        partage_tournoi(t, &t1, &t2);
        t = un_tour(t1, t2);
    }
    return t;
}

//12
int resolution_tournoi(liste l)
{
    int res = tete_liste(l);
    tournoi t = prepare_tournoi(l);

    detruire_tournoi(t);
    return res;
} 

int main()
{
}