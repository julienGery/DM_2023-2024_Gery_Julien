/*
CORRIGÉ PARTIEL DU TP : STRUCTURES RÉCURSIVES EN C
Par J. BENOUWT
Licence CC BY-NC-SA
*/

/* Implémentation d'une liste chaînée */

#ifndef _LISTE_H_
#define _LISTE_H_

#include "librairies.h"

// Définition des types
typedef int contenu;
typedef struct maillon_s maillon;
typedef maillon* liste;

// Interface principale
liste creer_liste(); // O(1)
bool est_vide_liste(liste); // O(1)
void ajouter_tete_liste(contenu, liste*); // O(1)
contenu tete_liste(liste); // O(1)
liste queue_liste(liste); // O(1)
void detruire_liste(liste); // O(taille(liste))

// Fonctions complémentaires
contenu acceder_indice_liste(liste, int);
void modifier_indice_liste(liste, int, contenu);
int longueur_liste(liste);
bool appartient_liste(liste, contenu);
void afficher_liste(liste);
bool egalite_liste(liste, liste);
liste copie_liste(liste);
bool est_triee_liste(liste);

// Plus de fonctionnalités
void inserer_indice_liste(liste* l, contenu, int);
void supprimer_indice_liste(liste* l, int);
liste concatener_liste(liste, liste);
liste tableau_vers_liste(contenu*, int);
contenu* liste_vers_tableau(liste);
liste renverser_liste(liste);

#endif

