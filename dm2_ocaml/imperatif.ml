type magique = Magique | None
exception Return_false

let print_matrix m =
    for j = 0 to Array.length m - 1 do
        for i = 0 to Array.length m.(j) - 1 do
            print_int m.(j).(i);
            print_string "\t";
        done;
        print_string "\n";
    done;
    print_string "\n"

(* on note n la taille de la liste:
    C = log_2 n
    C = O(log(n))
*)
let cherche l e =
    let bottom = ref 0 in
    let top = ref (Array.length l - 1) in
    let index = ref ((!bottom + !top) / 2) in

    while !bottom <= !top && e != l.(!index) do
        index := (!bottom + !top) / 2;
        if e <= l.(!index) then
            top := !index - 1
        else
            bottom := !index + 1;
    done;

    match l.(!index) = e with
        |true -> Some !index
        |false -> None


(* on note n la taille du carré magique,
    C = O(n ^ 2)
 *)
let constante_magique m =
    try
        let constante = ref 0 in

        let n = Array.length m in

        for i = 0 to n - 1 do
            constante := !constante + m.(0).(i)
        done;

        let diagonal_a = ref 0 in
        let diagonal_b = ref 0 in
        let somme_colone = Array.make (Array.length m) 0 in
        for j = 0 to Array.length m - 1 do
            let somme_ligne = ref 0 in

            if Array.length m.(j) != n then raise Return_false;

            for i = 0 to n - 1 do
                somme_ligne := !somme_ligne + m.(j).(i);
                somme_colone.(i) <- somme_colone.(i) + m.(j).(i);

                if i = j then
                    diagonal_a := !diagonal_a + m.(j).(i);
                if i = Array.length m - 1 - j then
                    diagonal_b := !diagonal_b + m.(j).(i);
            done;

            if !somme_ligne != !constante then
                raise Return_false;
        done;

        for i = 0 to Array.length somme_colone - 1 do
            if somme_colone.(i) != !constante then
                raise Return_false;
        done;

        if !diagonal_b != !constante || !diagonal_a != !constante then
            raise Return_false;

        Magique

    with Return_false -> None

(* on note n la taille du carré magique: *)
(*     C = O(n * n) *)

let est_normal m =
    let n = Array.length m in
    let nombres = Array.make (n * n - 1) 0 in

    for j = 0 to n - 1 do
        for i = 0 to n - 1 do
            let c = m.(j).(i) in
            if c < n * n then
                nombres.(c - 1) <- 1;
        done;
    done;

    let est_normal = ref true in
    for i = 0 to Array.length nombres - 1 do
        if nombres.(i) = 0 then
            est_normal := false;
    done;
    !est_normal


(* on note n la taille du carré magique: *)
(*     C = O(n * n) *)
let cree_carre_magique n =
    let carre = Array.make_matrix n n 0 in

    let i = ref (n / 2 + 1) in
    let j = ref 0 in
    let k = ref 1 in

    for c = 0 to n - 1 do
        if c != 0 then
            j := !j + 2;
        i := !i - 1;

        for d = 0 to n - 1 do
            if !j < 0 then
                j := !j + n;
            if !i < 0 then
                i := !i + n;

            carre.(!j).(!i) <- !k;

            k := !k + 1;
            j := (!j - 1) mod n;
            i := (!i + 1) mod n;
        done;
    done;
    carre

(* let carre_magique = [| *)
(*   	[|2; 7; 6|]; *)
(*   	[|9; 5; 1|]; *)
(*     [|4; 3; 8|]|];; *)
