let rec print_list a = match a with
    |[] -> print_string "\n"
    |h :: t -> print_int h ; print_string " " ; print_list t

let maximum l =
    let rec _maximum _l i m im = match _l with
        |[] -> im
        |h :: t -> match h >= m with
            |true -> _maximum t (i + 1) h i
            |false -> _maximum t (i + 1) m im in

    match l with
        |[] -> failwith "list vide!"
        |h ::t -> _maximum t 1 h 0

let rec take l n = match n with
    |0 -> []
    |n -> match l with
        |[] -> failwith "pas assez d'elements"
        |h :: t -> h :: take t (n - 1)

let rec drop l n = match n with
    |0 -> l
    |n -> match l with
        |[] -> failwith "pas assez d'elements"
        |h :: t -> drop t (n - 1)


let clamp t mini maxi =
    max mini (min maxi t)

let monotone _l =
    let rec _monotone l = match l with
        | [] | [_] | [_; _] -> true
        | x::y::z::xs -> (x <= y && y <= z || x >= y && y >= z) && _monotone (y::z::xs) in

    match _l with
        |[] | [_] -> 0
        |x :: y :: xs -> match _monotone (x :: y :: xs) with
                |true -> clamp (x - y) 0 1
                |false -> -1

let meme_signe a b =
    match clamp a (-1) 1, clamp b (-1) 1 with
        |0, _ | _, 0 | 1, 1 | -1, -1 -> true
        |_, _ -> false


let rec length l = match l with
    |[] -> 0
    |h :: t -> 1 + length t

let unimodale_v1 l =
    let split = maximum l - 1 in
    let a = take l split in
    let b = drop l split in

    match length b with
    |0 | 1 -> true
    |_ -> match monotone a, monotone b with
            |0, 1 -> true
            |_, _ -> false

let unimodale_v2 l =
    (* "renvoie le nombre d'extremums" *)
    let rec help _l m n = match _l with
        |[] -> n
        |[x] -> n
        |x::y::xs -> let diff = x - y in
                match meme_signe diff m with
                    |true -> help (y :: xs) diff n
                    |false -> help (y :: xs) diff (n + 1) in
    match l with
        |[] -> true
        |[x] -> true
        |x::y::xs -> help (y::xs) (x - y) 0 <= 1

