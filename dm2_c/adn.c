#include <stdbool.h>
#include <stdlib.h>

int length(char* a)
{
    int n = 1;
    while (a[n] != '\0')
        n++;
    return n;
}

char* new_string(const int n)
{
    char* rep = malloc((n + 1) * sizeof(char));
    rep[n] = '\0';
    return rep;
}

bool est_sequence_adn(char* adn)
{
    for(int i = 0; i < length(adn); i++)
        if (adn[i] != 'A' && adn[i] != 'T' && adn[i] != 'G' && adn[i] != 'C')
            return false;
    return true;
}

char nucloetide_complementaire(char nucloetide)
{
    if (nucloetide == 'A')
        return 'T';
    if (nucloetide == 'T')
        return 'A';
    if(nucloetide == 'G')
        return 'C';
    return 'G';
}

char* deuxieme_brin(char* brin)
{
    const int n = length(brin);
    char* brin_complementaire = new_string(n);
    for(int i = 0; i < n; i++)
        brin_complementaire[i] = nucloetide_complementaire(brin[n - 1 - i]);

    return brin_complementaire;
}

bool est_double_brin(char* brin_a, char* brin_b)
{
    const int n = length(brin_a);
    if(length(brin_b) != n)
        return false;

    for(int i = 0; i < n; i++)
        if(brin_a[i] != nucloetide_complementaire(brin_b[n - 1 - i]))
            return false;
    return true;
}

char* adn_vers_arn(char* adn)
{
    const int n = length(adn);
    char* arn = new_string(n);

    for(int i = 0; i < n; i++)
        if (adn[i] == 'T')
            arn[i] = 'U';
        else
            arn[i] = adn[i];

    return arn;
}

char** codons(char* adn)
{
    const int n = length(adn);
    char* arn = adn_vers_arn(adn);

    char** rep = malloc(n / 3 * sizeof(char*));

    for(int i = 0; i < n / 3; i++)
        rep[i] = new_string(3);

    for(int i = 0; i < n; i++)
        rep[i / 3][i % 3] = arn[i];

    free(arn);
    return rep;
}

//renvoie vrai si deux chaines sont égales.
bool codonscmp(char* a, char* b)
{
    const int n = length(a);
    if(length(b) != n)
        return false;

    for(int i= 0; i < n; i++)
        if(a[i] != b[i])
            return false;
    return true;
}

int nombre_occurences_codons(char* adn, char* codon)
{
    const int n = length(adn);

    char** cod = codons(adn);
    int nb = 0;
    for(int i = 0; i < n / 3; i++)
        if(codonscmp(cod[i], codon))
            nb++;

    free(cod);
    return nb;
}

bool proteine_dans_code_genetique(char* adn, char** proteine, int n)
{
    char* brin_complementaire = deuxieme_brin(adn);

    for(int i = 0; i <n; i++)
        if(nombre_occurences_codons(adn, proteine[i]) == 0 && nombre_occurences_codons(brin_complementaire, proteine[i]) == 0)
        {
            free(brin_complementaire);
            return false;
        }

    free(brin_complementaire);
    return true;
}
