#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "unistd.h"

void affiche_jeu(int x, int y, bool** jeu)
{
    for(int j = 0; j < y; j++)
    {
        for(int i = 0; i < x; i++)
            if (jeu[j][i] == 1)
                printf("X ");
            else
                printf(". ");
        printf("\n");
    }
    // printf("\n");
}

// void affiche_tableau(int x, int y, int* tableau)
// {
//     for(int i = 0; i < x * y; i++)
//     {
//         if (i && i%y == 0)
//             printf("\n");
//         printf("%d ", tableau[i]);
//     }
//     printf("\n\n");
// }

bool** init_jeu(int x, int y)
{
    srand(time(NULL));
    bool** tableau = malloc(y * sizeof(bool*));

    for(int j = 0; j < y; j++)
    {
        tableau[j] = malloc(x * sizeof(bool));
        for(int i = 0; i < x; i++)
            tableau[j][i] = rand() % 2;
    }

    return tableau;
}

int max(const int a, const int b)
{
    if (a < b)
        return b;
    return  a;
}

int min(const int a, const int b)
{
    if (a > b)
        return b;
    return  a;
}

int nb_voisines_vivantes(int x, int y, bool** jeu, int c_x, int c_y)
{
    const int start_x = max(0, c_x - 1);
    const int start_y = max(0, c_y - 1);
    const int end_x = min(x, c_x + 2);
    const int end_y = min(y, c_y + 2);

    int n = 0;
    for(int j = start_y; j < end_y; j++)
        for(int i = start_x; i < end_x; i++)
            if(jeu[j][i] == 1 && !(i == c_x && j == c_y))
                n++;
    return n;
}

void tour(int x, int y, bool** jeu)
{
    int* tableau = malloc(x * y * sizeof(int));
    for(int i = 0; i < x * y; i++)
        tableau[i] = 0;

    for(int j = 0; j < y; j++)
        for(int i = 0; i < x; i++)
            tableau[j * x + i] = nb_voisines_vivantes(x, y, jeu, i, j);

    for(int j = 0; j < y; j++)
        for(int i = 0; i < x; i++)
        {
            const int nombres_de_voisins = tableau[j * x + i];
            bool* cellule = &jeu[j][i];

            if (*cellule == false && nombres_de_voisins == 3)
                *cellule = true;
            else if (*cellule == true && (nombres_de_voisins == 2 || nombres_de_voisins == 3))
                *cellule = true;
            else
                *cellule = false;
        }

    free(tableau);
}

int main(int argc, char** argv)
{
    const int x = atoi(argv[1]);
    const int y = atoi(argv[2]);

    bool** jeu = NULL;
    for(;;)
    {
        jeu = init_jeu(x, y);
        affiche_jeu(x, y, jeu);
        char c;
        printf("cette configurations vous convient ? y(es)\n");
        scanf("%s", &c);
        if (c == 'y')
            break;
    }

    //pour des raisons étranges, sans cette ligne jeu n'est pas valide. (testé avec clang)
    if(jeu == NULL)
        return -1;

    system("clear");
    for(;;)
    {
        affiche_jeu(x, y, jeu);
        fflush(stdout);
        sleep(2);
        system("clear");
        tour(x, y, jeu);
    }
}
