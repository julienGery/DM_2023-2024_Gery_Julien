#include <stdio.h>
#include <stdlib.h>

void affiche_tab_chaines(char** chaines, int n)
{
    for(int i = 0; i < n; i++)
        printf("%s\n", chaines[i]);
    printf("\n");
}

char* new_string(const int n)
{
    char* rep = malloc((n + 1) * sizeof(char));
    for(int i = 0; i < n; i++)
        rep[i] = ' ';
    rep[n] = '\0';
    return rep;
}

char** tableau(const int n)
{
    char** sapin = malloc(n * sizeof(char*));
    for(int i = 0; i < n; i++)
        sapin[i] = new_string(n * 2);
    return sapin;
}

void fait_un_triangle(char** sapin, int n, char lettre, int offset)
{
    for(int j = 0; j < n; j++)
        for(int i = n - j - 1 + offset; i <= n - 1 + offset + j; i += 2)
            sapin[j][i] = lettre;
}

char** sapin_noel(int n)
{
    char** sapin = tableau(n + 2);

    fait_un_triangle(sapin, n, '*', 0);
    for(int i = 1; i <= n / 3; i++)
        fait_un_triangle(&sapin[i * 2], n - i * 3, 'a' + i - 1, i * 3);

    for(int i = n - 2; i < n + 1; i++)
    {
        sapin[n][i] = '|';
        sapin[n + 1][i] = '|';
    }

    return sapin;
}

char complementaire(char c)
{
    if (c == '\\')
        return '/';
    if (c == '/')
        return '\\';
    return c;
}

void mirroir(char** tab, const int n)
{
    const int millieu = n / 2;
    for(int j = 0; j < n; j++)
        for(int i = 0; i < millieu; i++)
            tab[j][n - 1 - i] = complementaire(tab[j][i]);
}

void mirroir_b(char** tab, const int n)
{
    const int millieu = n / 2;

    for(int j = 1; j < millieu; j++)
        for(int i = 1; i < n - 1; i++)
            if (tab[j - 1][i - 1] == '_' && tab[j][i] == '_')
                tab[n - 1 - j][i] = '_';
            else if (tab[j][i] == '_' && tab[j - 1][i] == '_')
                tab[n - 1 - j][i] = '_';
            else if (tab[j][i] != '_')
                tab[n - 1 - j][i] = complementaire(tab[j][i]);
}

char** flocon(const int n)
{
    const int size = 14 + (n - 1) * 4;
    char** flo = tableau(size);

    flo[size / 2 - 2 - n][size / 2 - n - 2] = '.';
    const int millieu = size / 2;
    for(int j = 0; j < n; j++)
    {
        const int ligne = size / 2 - 1 - n + j;
        const int colone = j - n + millieu - 2;
        flo[ligne][colone] = '_';
        flo[ligne][colone + 1] = '\\';
        flo[ligne][colone + 2] = '/';
    }

    flo[size / 2 - 1][0] = '.';
    for(int i = 1; i <= n + 1; i++)
    {
        flo[size / 2 - 1][i * 2] = '_';
        flo[size / 2 - 1][i * 2 + 1] = '\\';
    }

    flo[size / 2 - 1][(n + 2) * 2] = '/';
    mirroir_b(flo, size);
    mirroir(flo, size);

    return flo;
}

int main()
{
    const int taille = 4;
    affiche_tab_chaines(flocon(taille), (taille - 1) * 4 + 14);
    affiche_tab_chaines(sapin_noel(taille), taille + 2);
}
